# XDWC accounting

This project aims to account donations and spendings of XDWC (Xonotic Defrag World Cup) and related events.

## Format

The file `xdwc.data` contains the entire money history. It's in ledger format. To calculate the current balance you need [ledger](https://www.ledger-cli.org/) or [hledger](https://hledger.org/) tools. Refer to their documentation.

## Web UI report

The read-only web UI report can be launched using the provided docker image. Run the following command to build and run:

```
./run.sh --build
```

Then navigate your browser to http://127.0.0.1:5000/